news hub. a drupal 7 distribution.

you can build the news_hub install profile by doing the following:

cd path/to/drupal/profiles/news_hub
drush make --no-core --contrib-destination=. news_hub.make
